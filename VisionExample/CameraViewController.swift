//
//  Copyright (c) 2018 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import AVFoundation
import CoreVideo
import MLKit

@objc(CameraViewController)
class CameraViewController: UIViewController {
    private let detectors: [Detector] = [
        .onDeviceText,
    ]
    var allData = [Element]()
    var allElementsData = [Line]()
    var sortElements = [Element]()
    var aa = [(name: String, number: Double)]()
    var allRows: String = ""

    private var currentDetector: Detector = .onDeviceText
    private var isUsingFrontCamera = false
    private var previewLayer: AVCaptureVideoPreviewLayer!
    private lazy var captureSession = AVCaptureSession()
    private lazy var sessionQueue = DispatchQueue(label: Constant.sessionQueueLabel)
    private var lastFrame: CMSampleBuffer?
    private lazy var previewOverlayView: UIImageView = {

        precondition(isViewLoaded)
        let previewOverlayView = UIImageView(frame: .zero)
        previewOverlayView.contentMode = UIView.ContentMode.scaleAspectFill
        previewOverlayView.translatesAutoresizingMaskIntoConstraints = false
        return previewOverlayView
    }()

    private lazy var annotationOverlayView: UIView = {
        precondition(isViewLoaded)
        let annotationOverlayView = UIView(frame: .zero)
        annotationOverlayView.translatesAutoresizingMaskIntoConstraints = false
        return annotationOverlayView
    }()

    private var poseDetector: PoseDetector? = nil
    private var lastDetector: Detector?

    // MARK: - IBOutlets
    @IBOutlet private weak var cameraView: UIView!

    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        setUpPreviewOverlayView()
        setUpAnnotationOverlayView()
        setUpCaptureSessionOutput()
        setUpCaptureSessionInput()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startSession()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopSession()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        previewLayer.frame = cameraView.frame
    }

    // MARK: - IBActions
    @IBAction func selectDetector(_ sender: Any) {
        presentDetectorsAlertController()
    }

    private func recognizeTextOnDevice(in image: VisionImage, width: CGFloat, height: CGFloat) {
        var recognizedText: Text
        do {
            recognizedText = try TextRecognizer.textRecognizer().results(in: image)
        } catch let error {
            print("Failed to recognize text with error: \(error.localizedDescription).")
            return
        }
        weak var weakSelf = self
        DispatchQueue.main.sync {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            strongSelf.updatePreviewOverlayView()
            strongSelf.removeDetectionAnnotations()
            // Blocks.
            for block in recognizedText.blocks {
                // Lines.
                for line in block.lines {
                    // Elements.
                    for element in line.elements {
                        let normalizedRect = CGRect(
                            x: element.frame.origin.x / width,
                            y: element.frame.origin.y / height,
                            width: element.frame.size.width / width,
                            height: element.frame.size.height / height
                        )
                        let convertedRect = strongSelf.previewLayer.layerRectConverted(
                            fromMetadataOutputRect: normalizedRect
                        )
                        UIUtilities.addRectangle(
                            convertedRect,
                            to: strongSelf.annotationOverlayView,
                            color: UIColor.blue
                        )
                        self.allData.append(Element(number: false, name: element.text , frame: convertedRect, found: false))
                    }
                }
            }

            self.sortElements.removeAll()
            self.sortElements.append(contentsOf: self.allData.sorted(by: {$0.frame.minY < $1.frame.minY}))
            self.allData.removeAll()
            self.allRows = ""
            self.allElementsData.removeAll()
            while(self.sortElements.count > 0)
            {
                var rowsArray = Line(items: [])
                var numbers = [Int]()
                let height = self.sortElements[0].frame.height
                let newHeight = height * 1.6
                let newPivotPoint = ((self.sortElements[0].frame.maxY + self.sortElements[0].frame.minY) / 2)
                var lineBoundary = [(newMinY: CGFloat, newMaxY: CGFloat)]()
                lineBoundary.append((newMinY: newPivotPoint - (newHeight / 2), newMaxY: newPivotPoint + (newHeight / 2)))
                for i in 0..<self.sortElements.count
                {
                    let a = self.sortElements[i].frame.minY - lineBoundary[0].newMinY
                    let b = lineBoundary[0].newMaxY - self.sortElements[i].frame.maxY
                    let height = self.sortElements[i].frame.maxY - self.sortElements[i].frame.minY
                    var isInside: Bool = false
                    if a >= 0 && b >= 0 {
                        isInside = true
                    }
                    else if a < 0 && b >= 0 {
                        isInside = abs(Double(a) / Double(height)) <= 0.6
                    }
                    else if a < 0 && b < 0 {
                        isInside = false
                    }
                    else if a >= 0 && b < 0 {
                        isInside = abs(Double(b) / Double(height)) <= 0.6
                    }
                    if isInside {
                        rowsArray.items.append(self.sortElements[i])
                        numbers.append(i)
                    } else {
                        break
                    }
                }
                if numbers.count > 0 {
                    var newNumbers = [Int]()
                    newNumbers.append(contentsOf: numbers.sorted(by: { $0 < $1 }))
                    for index in 0...newNumbers.count - 1 {
                        self.sortElements.remove(at: newNumbers[index])
                        for z in (index + 1)..<(newNumbers.count)  {
                            if z != 0 {
                                newNumbers[z] -= 1
                            }
                        }
                    }
                }
                var lineData = [Element]()
                lineData.append(contentsOf: rowsArray.items.sorted(by: { $0.frame.minX < $1.frame.minX }))
                self.allElementsData.append(Line(items: lineData))
            }

            self.aa = self.getReceiptItems(receiptLines: self.allElementsData)

            for m in 0..<self.aa.count  {
                var item = ""
                if self.aa[m].name == "QR" {
                    let new = Int(self.aa[m].number)
                     item = self.aa[m].name + " => " + "\(new) "
                } else {
                    item = self.aa[m].name + " => " + "\(self.aa[m].number) ₺"
                }
                self.allRows += item + "\n"
            }

           if self.allRows != "" {
                strongSelf.showResults()
          }
        }
    }

    private func showResults() {
        let resultsAlertController = UIAlertController(
            title: "Detection Results",
            message: nil,
            preferredStyle: .actionSheet
        )
        resultsAlertController.addAction(
            UIAlertAction(title: "OK", style: .destructive) { _ in
                resultsAlertController.dismiss(animated: true, completion: nil)
            }
        )
        resultsAlertController.message = allRows
        resultsAlertController.popoverPresentationController?.sourceView = self.view
        present(resultsAlertController, animated: true, completion: nil)
    }

    private func containsMultipleDots(str: String?) -> Bool {
        if (str == nil) {
            return false
        }
        var count = 0
        var c: Character
        let charArr1 = [Character](str!)
        for element in 0..<charArr1.count {
            c = charArr1[element]
            if (c == "." || c == ",") {
                count += 1
            }
        }
        return count > 1
    }

    private func getReceiptItems(receiptLines: [Line]) -> [(name: String, number: Double)] {
        var receiptItems = [(name: String, number: Double)]()
        let numRegex =  try! NSRegularExpression(pattern: "\\d+[.,]\\d{2}")
        for line in receiptLines {
            var lastElement = line.items[line.items.count-1].name
            let decimalCharacters = CharacterSet.decimalDigits
            let decimalRange = lastElement.rangeOfCharacter(from: decimalCharacters)
            if decimalRange == nil {
                if lastElement.elementsEqual("TL") || lastElement.elementsEqual("tl") || lastElement.elementsEqual("₺") {
                    if line.items.count > 2 {
                        lastElement = line.items[line.items.count-2].name
                    }
                }
            }
            let number = getOnlyNumerics(str: lastElement) ?? ""
            let range = NSRange(location: 0, length: number.utf16.count)
            if  numRegex.firstMatch(in: number, options: [], range: range) != nil && !containsMultipleDots(str: number) {
                receiptItems.append((name: appendTexts(elements: line), number: Double(number.replacingOccurrences(of: ",", with: ".")) ?? 0))
            }
            // # QRCode
            let sharpLine = line.items[0].name
            var qrLine = ""
            if sharpLine.contains("#") {
                for item in line.items {
                    if item.name != "#" {
                        qrLine += item.name
                    }
                }
                if qrLine.count == 6 {
                    let decimalCharacters = CharacterSet.decimalDigits
                    let decimalRange = qrLine.rangeOfCharacter(from: decimalCharacters)
                    if decimalRange != nil {
                        receiptItems.append((name: "QR ", number: Double(qrLine) ?? 0))
                    }
                }
            }
        }
        return receiptItems
    }

    private func getOnlyNumerics(str: String?) -> String? {
        if (str == nil) {
            return nil
        }
        var strBuff = ""
        var c: Character
        let charArr1 = [Character](str!)
        for element in 0..<charArr1.count {
            c = charArr1[element]
            if (c.isNumber || c == "." || c == ",") {
                strBuff.append(c)
            }
        }
        return strBuff
    }

    private func appendTexts(elements: Line) -> String {
        var appendedText = ""
        elements.items.enumerated().forEach({ (index, element) in
            if index != elements.items.count-1 {
                appendedText += element.name + " "
            }
        })
        return appendedText
    }

    // MARK: - Private
    private func setUpCaptureSessionOutput() {
        weak var weakSelf = self
        sessionQueue.async {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            strongSelf.captureSession.beginConfiguration()
            strongSelf.captureSession.sessionPreset = AVCaptureSession.Preset.medium

            let output = AVCaptureVideoDataOutput()
            output.videoSettings = [
                (kCVPixelBufferPixelFormatTypeKey as String): kCVPixelFormatType_32BGRA
            ]
            output.alwaysDiscardsLateVideoFrames = true
            let outputQueue = DispatchQueue(label: Constant.videoDataOutputQueueLabel)
            output.setSampleBufferDelegate(strongSelf, queue: outputQueue)
            guard strongSelf.captureSession.canAddOutput(output) else {
                print("Failed to add capture session output.")
                return
            }
            strongSelf.captureSession.addOutput(output)
            strongSelf.captureSession.commitConfiguration()
        }
    }

    private func setUpCaptureSessionInput() {
        weak var weakSelf = self
        sessionQueue.async {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            let cameraPosition: AVCaptureDevice.Position = strongSelf.isUsingFrontCamera ? .back : .back
            guard let device = strongSelf.captureDevice(forPosition: cameraPosition) else {
                print("Failed to get capture device for camera position: \(cameraPosition)")
                return
            }
            do {
                strongSelf.captureSession.beginConfiguration()
                let currentInputs = strongSelf.captureSession.inputs
                for input in currentInputs {
                    strongSelf.captureSession.removeInput(input)
                }

                let input = try AVCaptureDeviceInput(device: device)
                guard strongSelf.captureSession.canAddInput(input) else {
                    print("Failed to add capture session input.")
                    return
                }
                strongSelf.captureSession.addInput(input)
                strongSelf.captureSession.commitConfiguration()
            } catch {
                print("Failed to create capture device input: \(error.localizedDescription)")
            }
        }
    }

    private func startSession() {
        weak var weakSelf = self
        sessionQueue.async {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            strongSelf.captureSession.startRunning()
        }
    }

    private func stopSession() {
        weak var weakSelf = self
        sessionQueue.async {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            strongSelf.captureSession.stopRunning()
        }
    }

    private func setUpPreviewOverlayView() {
        cameraView.addSubview(previewOverlayView)
        NSLayoutConstraint.activate([
            previewOverlayView.centerXAnchor.constraint(equalTo: cameraView.centerXAnchor),
            previewOverlayView.centerYAnchor.constraint(equalTo: cameraView.centerYAnchor),
            previewOverlayView.leadingAnchor.constraint(equalTo: cameraView.leadingAnchor),
            previewOverlayView.trailingAnchor.constraint(equalTo: cameraView.trailingAnchor),
        ])
    }

    private func setUpAnnotationOverlayView() {
        cameraView.addSubview(annotationOverlayView)
        NSLayoutConstraint.activate([
            annotationOverlayView.topAnchor.constraint(equalTo: cameraView.topAnchor),
            annotationOverlayView.leadingAnchor.constraint(equalTo: cameraView.leadingAnchor),
            annotationOverlayView.trailingAnchor.constraint(equalTo: cameraView.trailingAnchor),
            annotationOverlayView.bottomAnchor.constraint(equalTo: cameraView.bottomAnchor),
        ])
    }

    private func captureDevice(forPosition position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        if #available(iOS 10.0, *) {
            let discoverySession = AVCaptureDevice.DiscoverySession(
                deviceTypes: [.builtInWideAngleCamera],
                mediaType: .video,
                position: .unspecified
            )
            return discoverySession.devices.first { $0.position == position }
        }
        return nil
    }

    private func presentDetectorsAlertController() {
        let alertController = UIAlertController(
            title: Constant.alertControllerTitle,
            message: Constant.alertControllerMessage,
            preferredStyle: .alert
        )
        weak var weakSelf = self
        detectors.forEach { detectorType in
            let action = UIAlertAction(title: detectorType.rawValue, style: .default) {
                [unowned self] (action) in
                guard let value = action.title else { return }
                guard let detector = Detector(rawValue: value) else { return }
                guard let strongSelf = weakSelf else {
                    print("Self is nil!")
                    return
                }
                strongSelf.currentDetector = detector
                strongSelf.removeDetectionAnnotations()
            }
            if detectorType.rawValue == self.currentDetector.rawValue { action.isEnabled = false }
            alertController.addAction(action)
        }
        alertController.addAction(UIAlertAction(title: Constant.cancelActionTitleText, style: .cancel))
        present(alertController, animated: true)
    }

    private func removeDetectionAnnotations() {
        for annotationView in annotationOverlayView.subviews {
            annotationView.removeFromSuperview()
        }
    }

    private func updatePreviewOverlayView() {
        guard let lastFrame = lastFrame,
              let imageBuffer = CMSampleBufferGetImageBuffer(lastFrame)
        else {
            return
        }
        let ciImage = CIImage(cvPixelBuffer: imageBuffer)
        let context = CIContext(options: nil)
        guard let cgImage = context.createCGImage(ciImage, from: ciImage.extent) else {
            return
        }
        let rotatedImage = UIImage(cgImage: cgImage, scale: Constant.originalScale, orientation: .right)
        if isUsingFrontCamera {
            guard let rotatedCGImage = rotatedImage.cgImage else {
                return
            }
            let mirroredImage = UIImage(
                cgImage: rotatedCGImage, scale: Constant.originalScale, orientation: .leftMirrored)
            previewOverlayView.image = mirroredImage
        } else {
            previewOverlayView.image = rotatedImage
        }
    }

    private func convertedPoints(
        from points: [NSValue]?,
        width: CGFloat,
        height: CGFloat
    ) -> [NSValue]? {
        return points?.map {
            let cgPointValue = $0.cgPointValue
            let normalizedPoint = CGPoint(x: cgPointValue.x / width, y: cgPointValue.y / height)
            let cgPoint = previewLayer.layerPointConverted(fromCaptureDevicePoint: normalizedPoint)
            let value = NSValue(cgPoint: cgPoint)
            return value
        }
    }

    private func normalizedPoint(
        fromVisionPoint point: VisionPoint,
        width: CGFloat,
        height: CGFloat
    ) -> CGPoint {
        let cgPoint = CGPoint(x: point.x, y: point.y)
        var normalizedPoint = CGPoint(x: cgPoint.x / width, y: cgPoint.y / height)
        normalizedPoint = previewLayer.layerPointConverted(fromCaptureDevicePoint: normalizedPoint)
        return normalizedPoint
    }
}

extension CameraViewController: AVCaptureVideoDataOutputSampleBufferDelegate {

    func captureOutput(
        _ output: AVCaptureOutput,
        didOutput sampleBuffer: CMSampleBuffer,
        from connection: AVCaptureConnection
    ) {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            print("Failed to get image buffer from sample buffer.")
            return
        }
        lastFrame = sampleBuffer
        let visionImage = VisionImage(buffer: sampleBuffer)
        let orientation = UIUtilities.imageOrientation(
            fromDevicePosition: isUsingFrontCamera ? .back : .back
        )
        visionImage.orientation = orientation
        let imageWidth = CGFloat(CVPixelBufferGetWidth(imageBuffer))
        let imageHeight = CGFloat(CVPixelBufferGetHeight(imageBuffer))
        recognizeTextOnDevice(in: visionImage, width: imageWidth, height: imageHeight)
    }

    // MARK: - Constants
    public enum Detector: String {
        case onDeviceText = "Text Recognition"
    }

    private enum Constant {
        static let alertControllerTitle = "Vision Detectors"
        static let alertControllerMessage = "Select a detector"
        static let cancelActionTitleText = "Cancel"
        static let videoDataOutputQueueLabel = "com.google.mlkit.visiondetector.VideoDataOutputQueue"
        static let sessionQueueLabel = "com.google.mlkit.visiondetector.SessionQueue"
        static let noResultsMessage = "No Results"
        static let localModelFile = (name: "bird", type: "tflite")
        static let labelConfidenceThreshold = 0.75
        static let smallDotRadius: CGFloat = 4.0
        static let lineWidth: CGFloat = 3.0
        static let originalScale: CGFloat = 1.0
        static let padding: CGFloat = 10.0
        static let resultsLabelHeight: CGFloat = 200.0
        static let resultsLabelLines = 5
        static let imageLabelResultFrameX = 0.4
        static let imageLabelResultFrameY = 0.1
        static let imageLabelResultFrameWidth = 0.5
        static let imageLabelResultFrameHeight = 0.8
    }
}
