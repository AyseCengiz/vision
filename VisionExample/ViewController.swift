//
//  Copyright (c) 2018 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import MLKit
import UIKit
import Foundation

struct Element {
    let number: Bool
    let name: String
    let frame: CGRect
    let found: Bool
}
struct Line {
    var items: [Element]
}

@objc(ViewController)
class ViewController: UIViewController, UINavigationControllerDelegate {

    var resultsText = ""

    private lazy var annotationOverlayView: UIView = {
        precondition(isViewLoaded)
        let annotationOverlayView = UIView(frame: .zero)
        annotationOverlayView.translatesAutoresizingMaskIntoConstraints = false
        annotationOverlayView.clipsToBounds = true
        return annotationOverlayView
    }()

    var imagePicker = UIImagePickerController()
    var currentImage = 0
    private var poseDetector: PoseDetector? = nil

    // MARK: - IBOutlets
    @IBOutlet fileprivate weak var imageView: UIImageView!
    @IBOutlet fileprivate weak var photoCameraButton: UIBarButtonItem!
    @IBOutlet fileprivate weak var videoCameraButton: UIBarButtonItem!
    @IBOutlet weak var detectButton: UIBarButtonItem!

    var allData = [Element]()
    var allElementsData = [Line]()
    var sortElements = [Element]()
    var aa = [(name: String, number: Double)]()
    var allRows: String = ""

    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.image = UIImage(named: Constants.images[currentImage])
        imageView.addSubview(annotationOverlayView)
        NSLayoutConstraint.activate([
            annotationOverlayView.topAnchor.constraint(equalTo: imageView.topAnchor),
            annotationOverlayView.leadingAnchor.constraint(equalTo: imageView.leadingAnchor),
            annotationOverlayView.trailingAnchor.constraint(equalTo: imageView.trailingAnchor),
            annotationOverlayView.bottomAnchor.constraint(equalTo: imageView.bottomAnchor),
        ])
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary

        let isCameraAvailable =
            UIImagePickerController.isCameraDeviceAvailable(.front)
            || UIImagePickerController.isCameraDeviceAvailable(.rear)
        if isCameraAvailable {
            if #available(iOS 10.0, *) {
                videoCameraButton.isEnabled = true
            }
        } else {
            photoCameraButton.isEnabled = false
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
    }

    // MARK: - IBActions
    @IBAction func detect(_ sender: Any) {
        clearResults()
        allRows = ""
        detectTextOnDevice(image: imageView.image)
    }

    @IBAction func openPhotoLibrary(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true)
    }

    @IBAction func openCamera(_ sender: Any) {
        guard
            UIImagePickerController.isCameraDeviceAvailable(.front)
                || UIImagePickerController
                .isCameraDeviceAvailable(.rear)
        else {
            return
        }
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true)
    }

    @IBAction func changeImage(_ sender: Any) {
        clearResults()
        currentImage = (currentImage + 1) % Constants.images.count
        imageView.image = UIImage(named: Constants.images[currentImage])
    }

    @IBAction func downloadOrDeleteModel(_ sender: Any) {
        clearResults()
    }

    // MARK: - Private
    private func removeDetectionAnnotations() {
        for annotationView in annotationOverlayView.subviews {
            annotationView.removeFromSuperview()
        }
    }

    private func clearResults() {
        removeDetectionAnnotations()
        self.resultsText = ""
    }

    private func showResults() {
        let resultsAlertController = UIAlertController(
            title: "Detection Results",
            message: nil,
            preferredStyle: .actionSheet
        )
        resultsAlertController.addAction(
            UIAlertAction(title: "OK", style: .destructive) { _ in
                resultsAlertController.dismiss(animated: true, completion: nil)
            }
        )
        resultsAlertController.message = allRows
        resultsAlertController.popoverPresentationController?.barButtonItem = detectButton
        resultsAlertController.popoverPresentationController?.sourceView = self.view
        present(resultsAlertController, animated: true, completion: nil)

    }

    private func updateImageView(with image: UIImage) {
        let orientation = UIApplication.shared.statusBarOrientation
        var scaledImageWidth: CGFloat = 0.0
        var scaledImageHeight: CGFloat = 0.0
        switch orientation {
        case .portrait, .portraitUpsideDown, .unknown:
            scaledImageWidth = imageView.bounds.size.width
            scaledImageHeight = image.size.height * scaledImageWidth / image.size.width
        case .landscapeLeft, .landscapeRight:
            scaledImageWidth = image.size.width * scaledImageHeight / image.size.height
            scaledImageHeight = imageView.bounds.size.height
        @unknown default:
            fatalError()
        }
        weak var weakSelf = self
        DispatchQueue.global(qos: .userInitiated).async {
            var scaledImage = image.scaledImage(
                with: CGSize(width: scaledImageWidth, height: scaledImageHeight)
            )
            scaledImage = scaledImage ?? image
            guard let finalImage = scaledImage else { return }
            DispatchQueue.main.async {
                weakSelf?.imageView.image = finalImage
            }
        }
    }

    private func transformMatrix() -> CGAffineTransform {
        guard let image = imageView.image else { return CGAffineTransform() }
        let imageViewWidth = imageView.frame.size.width
        let imageViewHeight = imageView.frame.size.height
        let imageWidth = image.size.width
        let imageHeight = image.size.height

        let imageViewAspectRatio = imageViewWidth / imageViewHeight
        let imageAspectRatio = imageWidth / imageHeight
        let scale =
            (imageViewAspectRatio > imageAspectRatio)
            ? imageViewHeight / imageHeight : imageViewWidth / imageWidth

        let scaledImageWidth = imageWidth * scale
        let scaledImageHeight = imageHeight * scale
        let xValue = (imageViewWidth - scaledImageWidth) / CGFloat(2.0)
        let yValue = (imageViewHeight - scaledImageHeight) / CGFloat(2.0)

        var transform = CGAffineTransform.identity.translatedBy(x: xValue, y: yValue)
        transform = transform.scaledBy(x: scale, y: scale)
        return transform
    }

    private func pointFrom(_ visionPoint: VisionPoint) -> CGPoint {
        return CGPoint(x: visionPoint.x, y: visionPoint.y)
    }

    private func process(_ visionImage: VisionImage, with textRecognizer: TextRecognizer?) {
        weak var weakSelf = self
        textRecognizer?.process(visionImage) { text, error in
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            guard error == nil, let text = text else {
                let errorString = error?.localizedDescription ?? Constants.detectionNoResultsMessage
                strongSelf.resultsText = "Text recognizer failed with error: \(errorString)"
                strongSelf.showResults()
                return
            }
            // Blocks.
            for block in text.blocks {
                let transformedRect = block.frame.applying(strongSelf.transformMatrix())
                UIUtilities.addRectangle(
                    transformedRect,
                    to: strongSelf.annotationOverlayView,
                    color: UIColor.purple
                )
                // Lines.
                for line in block.lines {
                    let transformedRect = line.frame.applying(strongSelf.transformMatrix())
                    UIUtilities.addRectangle(
                        transformedRect,
                        to: strongSelf.annotationOverlayView,
                        color: UIColor.orange
                    )
                    // Elements.
                    for element in line.elements {
                        let transformedRect = element.frame.applying(strongSelf.transformMatrix())
                        UIUtilities.addRectangle(
                            transformedRect,
                            to: strongSelf.annotationOverlayView,
                            color: UIColor.green
                        )
                        self.allData.append(Element(number: false, name: element.text , frame: element.frame, found: false))
                    }
                }
            }

            self.sortElements.removeAll()
            self.sortElements.append(contentsOf: self.allData.sorted(by: {$0.frame.minY < $1.frame.minY}))
            self.allData.removeAll()
            self.allRows = ""
            self.allElementsData.removeAll()
            while(self.sortElements.count > 0)
            {
                var rowsArray = Line(items: [])
                var numbers = [Int]()
                let height = self.sortElements[0].frame.height
                let newHeight = height * 1.6
                let newPivotPoint = ((self.sortElements[0].frame.maxY + self.sortElements[0].frame.minY) / 2)
                var lineBoundary = [(newMinY: CGFloat, newMaxY: CGFloat)]()
                lineBoundary.append((newMinY: newPivotPoint - (newHeight / 2), newMaxY: newPivotPoint + (newHeight / 2)))

                for i in 0..<self.sortElements.count
                {
                    let a = self.sortElements[i].frame.minY - lineBoundary[0].newMinY
                    let b = lineBoundary[0].newMaxY - self.sortElements[i].frame.maxY
                    let height = self.sortElements[i].frame.maxY - self.sortElements[i].frame.minY
                    var isInside: Bool = false
                    if a >= 0 && b >= 0 {
                        isInside = true
                    }
                    else if a < 0 && b >= 0 {
                        isInside = abs(Double(a) / Double(height)) <= 0.6
                    }
                    else if a < 0 && b < 0 {
                        isInside = false
                    }
                    else if a >= 0 && b < 0 {
                        isInside = abs(Double(b) / Double(height)) <= 0.6
                    }
                    if isInside {
                        rowsArray.items.append(self.sortElements[i])
                        numbers.append(i)
                    } else {
                        break
                    }
                }
                if numbers.count > 0 {
                    var newNumbers = [Int]()
                    newNumbers.append(contentsOf: numbers.sorted(by: { $0 < $1 }))
                    for index in 0...newNumbers.count - 1 {
                        self.sortElements.remove(at: newNumbers[index])
                        for z in (index + 1)..<(newNumbers.count)  {
                            if z != 0 {
                                newNumbers[z] -= 1
                            }
                        }
                    }
                }
                var lineData = [Element]()
                lineData.append(contentsOf: rowsArray.items.sorted(by: { $0.frame.minX < $1.frame.minX }))
                self.allElementsData.append(Line(items: lineData))
            }

            self.aa = self.getReceiptItems(receiptLines: self.allElementsData)

            for m in 0..<self.aa.count  {
                var item = ""
                if self.aa[m].name == "QR" {
                    let new = Int(self.aa[m].number)
                     item = self.aa[m].name + " => " + "\(new) "
                } else {
                    item = self.aa[m].name + " => " + "\(self.aa[m].number) ₺"
                }
                self.allRows += item + "\n"
            }
            strongSelf.showResults()
        }
    }

    private func containsMultipleDots(str: String?) -> Bool {
      if (str == nil) {
        return false
      }
        var count = 0
        var c: Character
        let charArr1 = [Character](str!)
        for element in 0..<charArr1.count {
            c = charArr1[element]
            if (c == "." || c == ",") {
                count += 1
            }
        }
        return count > 1
    }

    private func getReceiptItems(receiptLines: [Line]) -> [(name: String, number: Double)] {
        var receiptItems = [(name: String, number: Double)]()
        let numRegex =  try! NSRegularExpression(pattern: "\\d+[.,]\\d{2}")
        for line in receiptLines {
            var lastElement = line.items[line.items.count-1].name
            let decimalCharacters = CharacterSet.decimalDigits
            let decimalRange = lastElement.rangeOfCharacter(from: decimalCharacters)
            if decimalRange == nil {
                if lastElement.elementsEqual("TL") || lastElement.elementsEqual("tl") || lastElement.elementsEqual("₺") {
                    if line.items.count > 2 {
                        lastElement = line.items[line.items.count-2].name
                    }
                }
            }
            let number = getOnlyNumerics(str: lastElement) ?? ""
            let range = NSRange(location: 0, length: number.utf16.count)
            if  numRegex.firstMatch(in: number, options: [], range: range) != nil && !containsMultipleDots(str: number) {
                receiptItems.append((name: appendTexts(elements: line), number: Double(number.replacingOccurrences(of: ",", with: ".")) ?? 0))
            }
            // # QRCode
            let sharpLine = line.items[0].name
            var qrLine = ""
            if sharpLine.contains("#") {
                for item in line.items {
                    if item.name != "#" {
                        qrLine += item.name
                    }
                }
                if qrLine.count == 6 {
                    let decimalCharacters = CharacterSet.decimalDigits
                    let decimalRange = qrLine.rangeOfCharacter(from: decimalCharacters)
                    if decimalRange != nil {
                        receiptItems.append((name: "QR ", number: Double(qrLine) ?? 0))
                    }
                }
            }
        }
        return receiptItems
    }

    private func getOnlyNumerics(str: String?) -> String? {
        if (str == nil) {
          return nil
        }
        var strBuff = ""
        var c: Character
        let charArr1 = [Character](str!)
        for element in 0..<charArr1.count {
            c = charArr1[element]
            if (c.isNumber || c == "." || c == ",") {
                strBuff.append(c)
            }
        }
        return strBuff
      }

      private func appendTexts(elements: Line) -> String
      {
        var appendedText = ""
        elements.items.enumerated().forEach({ (index, element) in
            if index != elements.items.count-1 {
                appendedText += element.name + " "
            }
        })
        return appendedText
      }
}

// MARK: - UIImagePickerControllerDelegate
extension ViewController: UIImagePickerControllerDelegate {

    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]
    ) {
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        clearResults()
        if let pickedImage =
            info[
                convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)]
            as? UIImage
        {
            updateImageView(with: pickedImage)
        }
        dismiss(animated: true)
    }
}

extension ViewController {
    func detectTextOnDevice(image: UIImage?) {
        guard let image = image else { return }
        let onDeviceTextRecognizer = TextRecognizer.textRecognizer()
        let visionImage = VisionImage(image: image)
        visionImage.orientation = image.imageOrientation
        self.resultsText += "Running On-Device Text Recognition...\n"
        process(visionImage, with: onDeviceTextRecognizer)
    }

}
// MARK: - Enums
private enum Constants {
    static let images = [
       "11.jpg","receipt.jpg","qq.jpg","pp.jpg","fatura.jpg", "aa.jpg", "bb.jpg", "cc.jpg", "dd.jpg","ee.jpg","ff.jpg"
    ]

    static let detectionNoResultsMessage = "No results returned."
    static let failedToDetectObjectsMessage = "Failed to detect objects in image."
    static let localModelFile = (name: "bird", type: "tflite")
    static let labelConfidenceThreshold = 0.75
    static let smallDotRadius: CGFloat = 5.0
    static let largeDotRadius: CGFloat = 10.0
    static let lineColor = UIColor.yellow.cgColor
    static let lineWidth: CGFloat = 3.0
    static let fillColor = UIColor.clear.cgColor
}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromUIImagePickerControllerInfoKeyDictionary(
    _ input: [UIImagePickerController.InfoKey: Any]
) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (key.rawValue, value) })
}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey)
-> String
{
    return input.rawValue
}
